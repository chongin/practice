# frozen_string_literal: true
require 'rdkafka'
require 'multi_json'
require 'ruby-kafka'
module Simulator
  def self.config
    {
      'service.kafka.host': "0.0.0.0:9090",
    }
  end

  module MessageCenter
    extend self

    def bootup
      @kafka ||= Kafka.new(
        seed_brokers: Simulator.config[:'service.kafka.host']
      )
    end

    def kafka_produce_message(kafka_message)
      bootup

      producer = @kafka.producer
      producer.produce(MultiJson.dump(kafka_message), topic: construct_topic('notification'))
      producer.deliver_messages
      producer.shutdown
      puts 11111111111111111111, kafka_message
    end

    def construct_topic(topic)
      "test_#{topic}"
    end
  end
end




def send_test_message(
  name = 'stream_thumbnail'
  Simulator::MessageCenter.kafka_produce_message(
    {
      channel: 'live_baccarat',
      name: name,
      level: 4,
      sender: 'mock_service',
      recipient: 'all',
      ts: Time.now.utc.to_ms,
      source_id: ULID.generate,
      data: {
        "feed_id": "KMOM6W5C",
        "name": "/lax/live_baccarat/feed_thumbnails/KMOM6W5C/KMOM6W5C-1623746628.jpg"
      }
    }
  )
end


send_test_message