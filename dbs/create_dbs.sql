CREATE DATABASE IF NOT EXISTS g2_pinion_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_pinion_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_axle_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_axle_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_nozzle_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_nozzle_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_rim_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_rim_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_steering_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_steering_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_screw_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_screw_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_seal_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_seal_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_spindle_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_spindle_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_tacho_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_tacho_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_vin_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_vin_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_fortuna_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_fortuna_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_barge_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_barge_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_batian_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_batian_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_lotterymon_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_lotterymon_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_blinker_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_blinker_test.* TO 'ias' identified by 'password';


CREATE DATABASE IF NOT EXISTS g2_registry_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_registry_test.* TO 'ias' identified by 'password';