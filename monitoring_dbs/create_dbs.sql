CREATE DATABASE IF NOT EXISTS g2_pinion_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_pinion_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_axle_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_axle_test.* TO 'ias' identified by 'password';

CREATE DATABASE IF NOT EXISTS g2_fortuna_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT ALL PRIVILEGES ON g2_fortuna_test.* TO 'ias' identified by 'password';
